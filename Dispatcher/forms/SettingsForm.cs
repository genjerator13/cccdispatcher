﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

using Dispatcher.Properties;

namespace Dispatcher.forms
{
    public partial class SettingsForm : Form
    {
        public int width;
        public int height;
        public int top;
        public int right;
        public int left;
        public int bottom;
        public SettingsForm()
        {            
            InitializeComponent();
            heightTextBox.Text = Properties.Settings.Default.height.ToString();
            widthTextBox.Text = Properties.Settings.Default.width.ToString();
            topTextBox.Text = Properties.Settings.Default.top.ToString();
            BottomTextBox.Text = Properties.Settings.Default.bottom.ToString();
            LeftTextBox.Text = Properties.Settings.Default.left.ToString();
            RightTextBox.Text = Properties.Settings.Default.right.ToString();
            landscapeCheckBox.Checked = Properties.Settings.Default.landscape;
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {            
            Properties.Settings.Default.height = Int32.Parse(heightTextBox.Text);
            Properties.Settings.Default.width = Int32.Parse(widthTextBox.Text);
            Properties.Settings.Default.top = Int32.Parse(topTextBox.Text);
            Properties.Settings.Default.bottom = Int32.Parse(BottomTextBox.Text);
            Properties.Settings.Default.left = Int32.Parse(LeftTextBox.Text);
            Properties.Settings.Default.right = Int32.Parse(RightTextBox.Text);
            Properties.Settings.Default.landscape = landscapeCheckBox.Checked;

            Properties.Settings.Default.Save();
            this.Close();
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
