﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System;
using System.Windows.Forms;
using NLog;
using Dispatcher.reports;
using System.Reflection;
using Dispatcher.forms;

namespace Dispatcher
{
    public partial class DispatcherMainForm : Form
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public BindingSource bs;
        public CCC_dev2Entities context;
        public bool activated ;
        public bool printingInProgress;
        public DateTime lastWorkerTime;
        DispatchCardReport dcr;
        public DispatcherMainForm()
        {
            try
            {
                logger.Info("DispatcherMainForm start");
                dcr = new DispatchCardReport();
                printingInProgress = false;
                activated = false;
                logger.Info("DispatcherMainForm context before");
                context = new CCC_dev2Entities();
                logger.Info("DispatcherMainForm context after");
                InitializeComponent();
                logger.Info("DispatcherMainForm InitializeComponent after");
                this.backgroundWorker1.RunWorkerAsync(5000);
                logger.Info("DispatcherMainForm RunWorkerAsync after");
                logger.Info("DispatcherMainForm end");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.StackTrace);
               logger.Error("DispatcherMainForm constructor exception"+ex.StackTrace);
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            var senderGrid = (DataGridView)sender;

            if (senderGrid.Columns[e.ColumnIndex] is DataGridViewButtonColumn &&
                e.RowIndex >= 0)
            {
                //TODO - Button Clicked - Execute Code Here
                Console.WriteLine("clicled");
                logger.Info("dataGridView1_CellContentClick");
                DispatchCardReport dcr = new DispatchCardReport();
                
                var id = (int)dataGridView1.Rows[e.RowIndex].Cells["id"].Value;
                dcr.ids.Add(id);
                //dcr.Show();
                logger.Info("Dispatch card report started");
            }
        }

        private void DispatcherMainForm_Load(object sender, EventArgs e)
        {
            try
            {
                int prg = Properties.Settings.Default.program;
                string txt = "ALL VEHICLES";
                if(prg==1)
                {
                    txt = "SMALL VEHICLES";
                }else if(prg==2){
                    txt = "LARGE TRUCKS VEHICLES";
                }


                this.Text = "Dispatcher Ver 08-12-2017 v1 --- " + txt;
                logger.Info("DispatcherMainForm_Load" + this.Text);
                this.refresh();
                
            }
            catch(Exception ex)
            {
                MessageBox.Show("Unable to connect to the remote Database", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Console.WriteLine(ex.StackTrace);
                logger.Error(ex.StackTrace);
                this.Dispose();
            }
        }

        public void refresh()
        {
            try
            {
                logger.Info("refresh() start" );
                //var test = from dc in from aa in context.Dispatchcards.AsEnumerable()
                //               select dc;


                
                
                //var q = from dc in context.Dispatchcards.OrderByDescending(p=>p.id).Take(10).ToList()
                int prg = Properties.Settings.Default.program;
                var q = from dc in context.Dispatchcards.OrderByDescending(p=>p.id)
                        join cust in context.Customers on dc.customer_id equals cust.id
                        join orig in context.origins on dc.id equals orig.dispatchcard_id
                        join dest in context.destinations on dc.id equals dest.dispatchcard_id
                        join vehtype in context.VehTypes on dest.vehicletype_id equals vehtype.id
                        where vehtype.prg == prg || prg==0
                        //join veht in context.VehTypes on dc.id equals veht.vehcode where veht.prg==1

                        //join cust in context.Customers on dc.customer_id equals cust.id
                        

                            
                //var q = from dc in context.Dispatchcards
                //        join cust in context.Customers on dc.customer_id equals cust.id
                //        //join orig in context.origins on dc.origins

                        select new
                        {
                            dc.dateorder,
                            dc.id,
                            call_in_by = dc.call_in_buy,
                            dc.serv_type,
                            customer = cust == null ? " " : cust.Name,
                            dc.po,
                            dc.cod_amount,
                            dc.comments,
                            dc.status,
                            pickup_building_business = orig.building_business,
                            delivery_building_business = dest.building_business,
                            originAddress = orig.address,
                            originPickupTime = orig.delivery_time,
                            destinationBuilding = dest.building_business,
                            destinationAddress = dest.address,
                            destinationPickupTime = dest.delivery_time,
                            destinationVehType = dest.VehType.VehDesc,// String.Join(", ", dc.destinations.Where(t => t.VehType != null).Select(t => t.VehType == null ? " " : t.VehType.VehDesc)),
                            pieces = dest.pieces,
                            weight = dest.weight
                        };

                //var dispatchCards2 = q; //.OrderByDescending(p=>p.id).ToList();
                //

                bs = new BindingSource();
                var list = q.OrderByDescending(p => p.id).Take(1500).ToList();
                bs.DataSource = list;
                if (prg == 0)
                {
                    int firstId = list.First().id;
                    int lastId = list.Last().id;
                    int listCount = list.Count;
                    int diff = firstId - lastId;
                    if (diff != listCount - 1)
                    {
                        Array al = list.ToArray();
                        int prevId = list.First().id;
                        foreach (var item in list)
                        {
                            if (prevId - item.id >= 2)
                            {
                                Console.WriteLine("ID is {0} and status is {1}", item.id, item.status);
                                MessageBox.Show("Missing ID :" + (item.id + 1), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                logger.Error("Missing ID :" + (item.id + 1));
                                origin orig = context.origins.First(i => i.dispatchcard_id == (item.id + 1));
                                destination dest = context.destinations.First(i => i.dispatchcard_id == (item.id + 1));
                                if (dest.vehicletype_id == null && orig.vehicletype_id != null)
                                {
                                    dest.vehicletype_id = orig.vehicletype_id;
                                    context.SaveChanges();
                                    logger.Error("Missing ID :" + (item.id + 1)+"PROBLEM with vehicle id :fixed");
                                }
                            }
                            prevId = item.id;
                        }


                    }
                }
                bindingNavigator1.BindingSource = bs;                
                dataGridView1.DataSource = bs;
                logger.Info("refresh() end");
            }catch(Exception ex){
                logger.Info("refresh() exception" + ex.StackTrace);
                Console.WriteLine(ex.StackTrace);
            }
            
        }

       

        public void updateSeen()
        {
            try
            {
                //var clean = from dc in db.Dispatchcards.ToList().Where(t=>t.status==null).Select(dc);
                var clean = from dc in context.Dispatchcards
                            where dc.status == null
                            select dc;
                foreach (Dispatchcard cleanCard in clean)
                {
                    cleanCard.status = 1;
                    // other changed properties
                    logger.Info("updateSeen() cleanCard id=" + cleanCard.id);
                }
                logger.Info("updateSeen() SaveChanges before " );
                context.SaveChanges();
                logger.Info("updateSeen() SaveChanges after ");
            }
            catch (Exception ex)
            {
                logger.Error("updateSeen() exception " + ex.StackTrace);
                this.backgroundWorker1.RunWorkerAsync(5000);
            }
            
        }
        public void checkNewDispatchCards()
        {
            Console.WriteLine("aaa");
        }

        private void bPrint_Click(object sender, EventArgs e)
        {
            dcr = new DispatchCardReport();
            foreach (DataGridViewRow r in dataGridView1.SelectedRows)
            {
                logger.Info("bPrint_Click id=" + r.Cells["id"].Value);
                dcr.ids.Add(r.Cells["id"].Value);
            }
            printDispatchCards(dcr);
            //dcr.printDispatchCards();
            //dcr.Show();
            logger.Info("bPrint_Click");
        }

        private void buttonNonPrinted_Click(object sender, System.EventArgs e)
        {
            logger.Info("printNonPrinted()");
            printNonPrinted();
        }
        public void printDispatchCards(DispatchCardReport dcr)
        {
            printingInProgress = true;
            dcr.printDispatchCards();
            printingInProgress = false;
        }
        public void printNonPrinted()
        {
            
            if(printingInProgress){
                return;
            }
            try
            {
                logger.Info("printNonPrinted() start");
                //dcr.printDispatchCards();
                //get dispatchcard ids where status=0
                
                dcr = new DispatchCardReport();                
                var db = new CCC_dev2Entities();
                int prg = Properties.Settings.Default.program;
                var nonprinted = from dc in db.Dispatchcards
                                 
                                 join dest in db.destinations on dc.id equals dest.dispatchcard_id
                                 join vehtype in db.VehTypes on dest.vehicletype_id equals vehtype.id
                                 where (dc.status != 2 || dc.status == null) && (vehtype.prg == prg || prg==0)
                                 select new
                                 {
                                     dc.id
                                 };
                Console.WriteLine(nonprinted.ToList());

                foreach (var item in nonprinted.ToList())
                {

                    //var q = from dc in context.Dispatchcards                            
                    //    join dest in context.destinations on dc.id equals dest.dispatchcard_id
                    //    join vehtype in context.VehTypes on dest.vehicletype_id equals vehtype.id
                    //    where vehtype.prg == prg && dc.id == item.id
                    //        select new
                    //        {
                    //            dc.dateorder,
                    //            dc.id,
                    //            call_in_by = dc.call_in_buy,
                    //            dc.serv_type,
                    //            destinationBuilding = dest.building_business,
                    //            destinationAddress = dest.address,
                    //            destinationPickupTime = dest.delivery_time,
                    //            destinationVehType = dest.VehType.VehDesc,// String.Join(", ", dc.destinations.Where(t => t.VehType != null).Select(t => t.VehType == null ? " " : t.VehType.VehDesc)),
                    //            pieces = dest.pieces,
                    //            weight = dest.weight
                    //        };

                    //Console.WriteLine(q.ToList());
                    logger.Info("printNonPrinted() id=" + item.id);
                    //if(q.ToList().Any()){
                        dcr.ids.Add(item.id);
                    //}
                }
                if(dcr.ids.Count>0){
                    printDispatchCards(dcr);
                }
                //dcr.printDispatchCards();
                //dcr.Show();
                logger.Info("printNonPrinted() end");
            }
            catch (Exception ex)
            {
                
                logger.Error("Exception" + ex.StackTrace);
            }
        }

        private void buttonPrintAll_Click(object sender, System.EventArgs e)
        {
            //DispatchCardReport dcr = new DispatchCardReport();
            //get dispatchcard ids where status=0
            var db = new CCC_dev2Entities();
            var nonprinted = from dc in db.Dispatchcards.ToList()
                             
                             select new
                             {
                                 dc.id
                             };
            foreach (var item in nonprinted)
            {
                dcr.ids.Add(item.id);
            }
            //dcr.Show();
            printDispatchCards(dcr);
            //dcr.printDispatchCards();
            //dcr.Show();
            logger.Info("Dispatch card report started");
        }

        private void DispatcherMainForm_Activated(object sender, System.EventArgs e)
        {
            activated = true;
            Console.WriteLine("activate");
            logger.Info("DispatcherMainForm_Activated");
            //this.updateSeen();
        }

        private void dataGridView1_DataBindingComplete(object sender, DataGridViewBindingCompleteEventArgs e)
        {
            Console.WriteLine("test");
            logger.Info("dataGridView1_DataBindingComplete");
            
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            
                logger.Info("backgroundWorker1_DoWork");
                while (true)
                {
                    try
                    {
                            System.Threading.Thread.Sleep(10000);
                            if (activated)
                            {
                                logger.Info("backgroundWorker1_DoWork activated activated");
                                context = new CCC_dev2Entities();
                                bool testdb = context.DatabaseExists();
                                int prg = Properties.Settings.Default.program;
                                Console.WriteLine(testdb);
                                var clean = from dc in context.Dispatchcards
                                            join dest in context.destinations on dc.id equals dest.dispatchcard_id
                                            join vehtype in context.VehTypes on dest.vehicletype_id equals vehtype.id
                                            where dc.status == null && (vehtype.prg == prg || prg == 0)
                                            select dc;
                                if (clean != null && clean.Any())
                                {
                                    //MethodInfo createRefreshMethod = this.GetType().GetMethod("refresh");
                                    //createRefreshMethod.Invoke(this, new object[] { });
                            
                                        BeginInvoke((MethodInvoker)delegate
                                        {
                                            logger.Info("backgroundWorker1_DoWork activated activated BeginInvoke");
                                            logger.Info("backgroundWorker1_DoWork activated activated BeginInvoke printNonPrinted refresh");
                                            this.refresh();
                                            logger.Info("backgroundWorker1_DoWork activated activated BeginInvoke printNonPrinted");
                                   
                                            this.printNonPrinted();
                                    
                                        });
                            
                                }
                            }//end activated
                    
                            //Console.WriteLine("WWOOORKAGAIN");
                            //if (!this.IsHandleCreated)
                            //{
                                BeginInvoke((MethodInvoker)delegate
                                {
                                    lastWorkerTime = DateTime.Now;
                                    labelStatus.Text = "Last time checked:   " + lastWorkerTime;
                                    labelPrinting.Text = printingInProgress ? "Printing" : "Not Printing";

                                });
                            //}
                            logger.Info("backgroundWorker1_DoWork worker working" + lastWorkerTime);
                    }
                    catch (Exception ex)
                    {
                        logger.Error("backgroundWorker1_DoWorkexception" + ex.StackTrace);

                        Console.WriteLine(ex.StackTrace);
                    }
                }
            
        }

        private void buttonRefresh_Click(object sender, System.EventArgs e)
        {
            this.refresh();
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            SettingsForm sf = new SettingsForm();
            sf.Show();
        }
    }
}
