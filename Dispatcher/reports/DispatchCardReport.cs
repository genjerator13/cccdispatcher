﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using Microsoft.Reporting.WinForms;
using System.Drawing.Printing;
using System.Drawing.Imaging;
using NLog;


namespace Dispatcher.reports
{
    public partial class DispatchCardReport : Form
    {
        public ArrayList ids = new ArrayList();
        private int m_currentPageIndex;
        private IList<Stream> m_streams;
        public bool printingInProgress;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        public bool first = true;
        public DispatchCardReport()
        {
            logger.Info("DispatchCardReport InitializeComponent");
            InitializeComponent();
            logger.Info("DispatchCardReport InitializeComponent after");
        }

        private void DispatchCardReport_Load(object sender, EventArgs e)
        {
            logger.Info("DispatchCardReport_Load start");

        }

        public void printDispatchCards()
        {
            try
            {
                logger.Info("printDispatchCards");
                printingInProgress = true;
                var context = new CCC_dev2Entities();
                var db = new CCC_dev2Entities();

                IQueryable<Dispatchcard> filteredDispatchcards = db.Dispatchcards;
                //if (printNonPrinted)
                //{
                //    filteredDispatchcards = filteredDispatchcards.Where(d => d.status != null);
                //}
                List<int> idslist = null;
                int prg = Properties.Settings.Default.program;
                if (ids.Count > 0)
                {
                    idslist = ids.Cast<int>().ToList();
                    filteredDispatchcards = filteredDispatchcards.Where(a => idslist.Contains(a.id));


                    var query = (from dc in filteredDispatchcards
                                 orderby dc.status descending
                                 select new
                                 {
                                     DC = dc,
                                     csr_name = dc.csr_name,
                                     Origins = dc.origins,
                                     Destinations = dc.destinations,
                                     Customer = dc.Customer,
                                     VehiceTypes = dc.destinations.Select(dest => new
                                     {
                                         DestId = dest.id,
                                         VehicleShortType = dest.VehType.short_type
                                     })
                                 })
                            .AsEnumerable()
                            .Select(dc => new
                            {
                                DC = dc.DC,
                                csr_name = dc.csr_name,
                                Origins = dc.Origins,
                                Destinations = dc.Destinations,
                                Customer = dc.Customer,
                                VehicleTypesDict = dc.VehiceTypes.ToDictionary(vt => vt.DestId, vt => vt.VehicleShortType)
                            });

                    string timeFormat = "hh:mm";
                    logger.Info("var dispatchCards2 = query.SelectMany");
                    var dispatchCards2 = query.SelectMany(dc =>
                            dc.Origins.Count > 1
                            ? dc.Origins.Select(orig =>
                                new
                                {
                                    dc.DC.dateorder,
                                    dc.DC.id,
                                    csr_name = dc.csr_name,
                                    dc.DC.serv_type,
                                    Customer = dc.DC.Customer == null ? " " : dc.Customer.CustCode,
                                    call_in_buy = dc.DC.call_in_buy ?? " ",
                                    po = dc.Destinations.FirstOrDefault() == null ? " " : dc.Destinations.FirstOrDefault().po,
                                    cod_amount = dc.Destinations.FirstOrDefault() == null ? 0 : dc.Destinations.FirstOrDefault().cod_amount,
                                    comments = dc.Destinations.FirstOrDefault() == null ? " " : dc.Destinations.FirstOrDefault().comments,

                                    // origin 
                                    pickup_building_business = orig.building_business,
                                    pickup_address = orig.address,
                                    pickup_contact_address = orig.address,
                                    pickup_contact_person = orig.contact_person,
                                    //requested_pickup_time = String.Join(", ", dc.origins.Select(t => t.delivery_time).AsEnumerable()),
                                    requested_pickup_time = orig.time_flag == true ? orig.delivery_time.Value.ToString(timeFormat) : "",

                                    // destination
                                    delivery_building_business = dc.Destinations.FirstOrDefault() == null ? " " : dc.Destinations.FirstOrDefault().building_business,
                                    commodity_instruction      = dc.Destinations.FirstOrDefault() == null ? false : dc.Destinations.FirstOrDefault().collect,

                                    delivery_address = dc.Destinations.FirstOrDefault() == null ? " " : dc.Destinations.FirstOrDefault().address,
                                    delivery_contact_person = dc.Destinations.FirstOrDefault() == null ? " " : dc.Destinations.FirstOrDefault().address,
                                    //requested_delivery_time = String.Join(", ", dc.destinations.Select(t => t.delivery_time).AsEnumerable()),
                                    requested_delivery_time = dc.Destinations.FirstOrDefault() == null || dc.Destinations.FirstOrDefault().time_flag == true ? dc.Destinations.FirstOrDefault().delivery_time.Value.ToString(timeFormat) : "",
                                    //status = String.Join(", ", dc.destinations.Where(t => t.VehType != null).Select(t => t.VehType == null ? " " : t.VehType.VehDesc)),
                                    status = dc.VehicleTypesDict.ContainsKey(dc.Destinations.FirstOrDefault().id)
                                                    ? (dc.VehicleTypesDict[dc.Destinations.FirstOrDefault().id] ?? string.Empty)
                                                    : string.Empty,
                                    weight = dc.Destinations.FirstOrDefault() == null ? 0 : dc.Destinations.FirstOrDefault().weight ?? 0,
                                    pieces = dc.Destinations.FirstOrDefault() == null ? 0 : dc.Destinations.FirstOrDefault().pieces ?? 0,
                                    date_created = dc.DC.date_created,
                                })
                                : dc.Destinations.Select(dest =>
                                new
                                {
                                    dc.DC.dateorder,
                                    dc.DC.id,
                                    csr_name = dc.csr_name,
                                    dc.DC.serv_type,
                                    Customer = dc.DC.Customer == null ? " " : dc.Customer.CustCode,
                                    call_in_buy = dc.DC.call_in_buy ?? " ",
                                    po = dest.po,
                                    cod_amount = dest.cod_amount,
                                    comments = dest.comments,

                                    // origin 
                                    pickup_building_business = dc.Origins.FirstOrDefault() == null ? " " : dc.Origins.FirstOrDefault().building_business,
                                    pickup_address = dc.Origins.FirstOrDefault() == null ? " " : dc.Origins.FirstOrDefault().address,
                                    pickup_contact_address = dc.Origins.FirstOrDefault() == null ? " " : dc.Origins.FirstOrDefault().address,
                                    pickup_contact_person = dc.Origins.FirstOrDefault() == null ? " " : dc.Origins.FirstOrDefault().contact_person,
                                    //requested_pickup_time = String.Join(", ", dc.origins.Select(t => t.delivery_time).AsEnumerable()),

                                    //old//requested_pickup_time = dc.Origins.FirstOrDefault().delivery_time.Value.ToString(timeFormat),
                                    requested_pickup_time = dc.Origins.FirstOrDefault() == null ? " " : dc.Origins.FirstOrDefault().time_flag == true ? dc.Origins.FirstOrDefault().delivery_time.Value.ToString(timeFormat) : "",




                                    // destination
                                    delivery_building_business = dest.building_business,
                                    commodity_instruction = dest.collect,
                                    delivery_address = dest.address,
                                    
                                    delivery_contact_person = dest.contact_person,
                                    //requested_delivery_time = String.Join(", ", dc.destinations.Select(t => t.delivery_time).AsEnumerable()),
                                    //old//requested_delivery_time = dest.delivery_time.Value.ToString(timeFormat),
                                    requested_delivery_time = dest.time_flag == true ? dest.delivery_time.Value.ToString(timeFormat) : "",

                                    //status = String.Join(", ", dc.destinations.Where(t => t.VehType != null).Select(t => t.VehType == null ? " " : t.VehType.VehDesc)),
                                    status = dc.VehicleTypesDict.ContainsKey(dest.id) ?
                                                (dc.VehicleTypesDict[dest.id] ?? string.Empty)
                                                : string.Empty,
                                    weight = dest.weight ?? 0,
                                    pieces = dest.pieces ?? 0,
                                    date_created = dc.DC.date_created,
                                }))
                                .ToList();
                    logger.Info("END var dispatchCards2 = query.SelectMany");








                    ////////
                    //PageSettings ps = new PageSettings(); //Declare a new PageSettings for printing
                    //ps.Landscape = Properties.Settings.Default.landscape; //Set True for landscape, False for Portrait
                    //ps.Margins = new Margins(Properties.Settings.Default.left, Properties.Settings.Default.right, Properties.Settings.Default.top, Properties.Settings.Default.bottom); //Set margins

                    //PaperSize paperSize = new PaperSize("MyCustom", Properties.Settings.Default.width, Properties.Settings.Default.height);
                    //paperSize.RawKind = (int)PaperKind.;                
                    //ps.PaperSize = paperSize;
                    //reportViewer1.SetPageSettings(ps);
                    reportViewer1.PrinterSettings.DefaultPageSettings.PaperSource.RawKind = (int)PaperKind.Custom;
                    //reportViewer1.PrinterSettings.DefaultPageSettings.PaperSize = paperSize;
                    logger.Info("END 174");
                    //reportViewer1.PrinterSettings.DefaultPageSettings.PaperSize.Width = Properties.Settings.Default.width; ;
                    //reportViewer1.PrinterSettings.DefaultPageSettings.PaperSize.Height = Properties.Settings.Default.height; ;
                    reportViewer1.PrinterSettings.DefaultPageSettings.Margins.Bottom = Properties.Settings.Default.bottom; ;
                    logger.Info("END 179");
                    reportViewer1.PrinterSettings.DefaultPageSettings.Margins.Top = Properties.Settings.Default.top;
                    reportViewer1.PrinterSettings.DefaultPageSettings.Margins.Left = Properties.Settings.Default.left;
                    reportViewer1.PrinterSettings.DefaultPageSettings.Margins.Right = Properties.Settings.Default.right;
                    logger.Info("END 183");
                    reportViewer1.PrinterSettings.DefaultPageSettings.Landscape = Properties.Settings.Default.landscape;
                    reportViewer1.LocalReport.GetDefaultPageSettings().Margins.Top = Properties.Settings.Default.top;
                    reportViewer1.LocalReport.GetDefaultPageSettings().Margins.Left = Properties.Settings.Default.left;
                    logger.Info("END 188");
                    reportViewer1.LocalReport.GetDefaultPageSettings().Margins.Right = Properties.Settings.Default.right;
                    reportViewer1.LocalReport.GetDefaultPageSettings().Margins.Bottom = Properties.Settings.Default.bottom;
                    logger.Info("END 191");
                    //reportViewer1.LocalReport.GetDefaultPageSettings().PaperSize.Width = Properties.Settings.Default.width;
                    //reportViewer1.LocalReport.GetDefaultPageSettings().PaperSize.Height = Properties.Settings.Default.height;
                    logger.Info("END 194");
                    //reportViewer1.LocalReport.GetDefaultPageSettings().IsLandscape = Properties.Settings.Default.landscape;
                    //reportViewer1.SetPageSettings(ps);

                    logger.Info("END 192");
                    ////////

                    this.reportViewer1.LocalReport.ReportEmbeddedResource = "Dispatcher.reports.DispatchCardReportLandscape.rdlc";
                    if (Properties.Settings.Default.landscape)
                    {
                        this.reportViewer1.LocalReport.ReportEmbeddedResource = "Dispatcher.reports.DispatchCardReport.rdlc";

                    }
                    logger.Info("END 204");
                    dispatchcardBindingSource.DataSource = dispatchCards2;
                    //direct printing
                    Export(this.reportViewer1.LocalReport);
                    Print();
                    printingInProgress = false;
                    //reportViewer1.RefreshReport();
                    if (idslist != null) // set as printed
                    {
                        var cleans = from dc in context.Dispatchcards
                                     where idslist.Contains(dc.id)
                                     select dc;
                        foreach (Dispatchcard ddc in cleans)
                        {
                            ddc.status = 2;
                        }
                        context.SaveChanges();

                    }
                    logger.Info("END 219");
                    //this.Close();
                }
            }
            catch (Exception ex)
            {
                logger.Info("EXCEPTION printcard");
                logger.Error(ex.StackTrace);
                Console.WriteLine(ex.StackTrace);
            }
        }



        // Routine to provide to the report renderer, in order to
        //    save an image for each page of the report.
        private Stream CreateStream(string name,
          string fileNameExtension, Encoding encoding,
          string mimeType, bool willSeek)
        {
            Stream stream = new MemoryStream();
            m_streams.Add(stream);
            logger.Info("CreateStream() end");
            return stream;
            
        }
        // Export the given report as an EMF (Enhanced Metafile) file.
        private void Export(LocalReport report)
        {
            try
            {
                first = false;
                string deviceInfo =
                  @"<DeviceInfo>
                <OutputFormat>EMF</OutputFormat>
                <PageWidth>5.5cm</PageWidth>
                <PageHeight>8.5cm</PageHeight>
                <MarginTop>1mm</MarginTop>
                <MarginLeft>1mm</MarginLeft>
                <MarginRight>1mm</MarginRight>
                <MarginBottom>4mm</MarginBottom>
            </DeviceInfo>";
                Warning[] warnings;
                m_streams = new List<Stream>();
                report.Render("Image", deviceInfo, CreateStream, out warnings);
                foreach (Stream stream in m_streams)
                    stream.Position = 0;
                logger.Info("Export(LocalReport report)");
            }
            catch (Exception ex)
            {
                logger.Error("Export(LocalReport report)" + ex.StackTrace);
            }
        }
        // Handler for PrintPageEvents
        private void PrintPage(object sender, PrintPageEventArgs ev)
        {
            try
            {
                Metafile pageImage = new
                   Metafile(m_streams[m_currentPageIndex]);

                // Adjust rectangular area with printer margins.
                Rectangle adjustedRect = new Rectangle(
                    ev.PageBounds.Left - (int)ev.PageSettings.HardMarginX,
                    ev.PageBounds.Top - (int)ev.PageSettings.HardMarginY,
                    ev.PageBounds.Width,
                    ev.PageBounds.Height);

                // Draw a white background for the report
                ev.Graphics.FillRectangle(Brushes.White, adjustedRect);

                // Draw the report content
                ev.Graphics.DrawImage(pageImage, adjustedRect);

                // Prepare for the next page. Make sure we haven't hit the end.
                m_currentPageIndex++;
                ev.HasMorePages = (m_currentPageIndex < m_streams.Count);
                logger.Info("print Page end");
            }
            catch (Exception ex)
            {
                logger.Error("print Page exception"+ex.StackTrace);
            }
        }
        public void Dispose()
        {
            try
            {
                logger.Info("Dispose");
                if (m_streams != null)
                {
                    foreach (Stream stream in m_streams)
                        stream.Close();
                    logger.Info("Dispose m_streams close");
                    m_streams = null;
                }
            }
            catch (Exception Ex)
            {
                logger.Error("Exception in Dispose()");
            }

        }

        private void Print()
        {
            try
            {
                if (m_streams == null || m_streams.Count == 0)
                {
                    logger.Error("Error: no stream to print.");
                    throw new Exception("Error: no stream to print.");
                }
                PrintDocument printDoc = new PrintDocument();
                if (!printDoc.PrinterSettings.IsValid)
                {
                    logger.Error("Error: cannot find the default printer.");
                    throw new Exception("Error: cannot find the default printer.");
                }
                else
                {
                    printDoc.PrintPage += new PrintPageEventHandler(PrintPage);
                    m_currentPageIndex = 0;
                    printDoc.Print();
                    logger.Info("end Print()");
                }
            }
            catch (Exception Ex)
            {
                logger.Error("Exception in Print()"+Ex.StackTrace);
            }
        }
    }
}
