﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.ComponentModel;
using System.Data.EntityClient;
using System.Data.Objects;
using System.Data.Objects.DataClasses;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;

[assembly: EdmSchemaAttribute()]
namespace Dispatcher
{
    #region Contexts
    
    /// <summary>
    /// No Metadata Documentation available.
    /// </summary>
    public partial class ccc_testEntities1 : ObjectContext
    {
        #region Constructors
    
        /// <summary>
        /// Initializes a new ccc_testEntities1 object using the connection string found in the 'ccc_testEntities1' section of the application configuration file.
        /// </summary>
        public ccc_testEntities1() : base("name=ccc_testEntities1", "ccc_testEntities1")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new ccc_testEntities1 object.
        /// </summary>
        public ccc_testEntities1(string connectionString) : base(connectionString, "ccc_testEntities1")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        /// <summary>
        /// Initialize a new ccc_testEntities1 object.
        /// </summary>
        public ccc_testEntities1(EntityConnection connection) : base(connection, "ccc_testEntities1")
        {
            this.ContextOptions.LazyLoadingEnabled = true;
            OnContextCreated();
        }
    
        #endregion
    
        #region Partial Methods
    
        partial void OnContextCreated();
    
        #endregion
    
    }

    #endregion

    
}
